// contain business logic, operations, query and manipulate doon sa index.js
// require model na may access sa collection w/s si task also sa shema

// It will allow us to use the contents of the task.js file in the model folder.
const Task = require("../models/task");

//Get all task
module.exports.getAllTasks = () =>{
	
	// the "then" method is used to wait for the mongoose "find" method to finish before sending the result back to the route and eventually to the client/postman.
	//pag tinawag cxa yung getAllTask sa taskroutes mkuha cxa dto sa find
	return Task.find({}).then(result => result); //yung result ibabalik nya 
}

//Create a task
module.exports.createTask = (reqBody) =>{

	//Create a task object based on the mongoose model "Task"
	let newTask = new Task({
		//Sets the "name" property with the value received from the postman.
		name: reqBody.name
	})
		//Using callback function: newUser.save((saveErr, savedTask) => {})
		//Using .then method: newTask.save.then((Task, error) =>{}) - eto na gagamitin
	return newTask.save().then((task, error) =>{
		if(error){
			console.log(error) //list down of errors sa postman
			return false
		}
		else{
			//Returns the new task object saved in the database back to the postman
			return task //isend yung newly save 
		}
	})
}

//Delete a task 
// Business Logic
		/*
			1. Look for the task with the corresponding id provided in the URL/route
			2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
		*/
module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>{
		if(err){
			console.log(err);
			return false; //either true or false, for now false muna kasi GUI and postman pa lng
		}
		else{
			return removedTask; // returns an object
		}
	})
} 

// Update a Task
// Business Logic
		/*
			1. Get the task with the id using the Mongoose method "findById"
			2. Replace the task's name returned from the database with the "name" property from the request body
			3. Save the task
		*/
							// from params, reqBody(guinamit natin sa taskRoutes)
module.exports.updateTask = (taskId, reqBody) =>{
	//findById is the saame as "find({"_id":"value"})"
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			//We reassign the result name with the request body content.
			result.name = reqBody.name;

			return result.save().then((updatedTaskName, updateErr) =>{
				if(updateErr){
					console.log(updateErr);
					return false;
				}
				else{
					return updatedTaskName;
				}
			})
		}
	})
}

//Activity
/*
Instructions s31 Activity:
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.
*/


// Get a specific task
module.exports.getSpecificTasks = (taskId) =>{
	
	return Task.findById(taskId).then((result, error) =>{

		if(error){
			console.log(error);
			return false;
		}
		else{
			return result;
		}
			
		})
}

//Changing the status of the task to complete
module.exports.updateTaskStatus = (taskId) => {
	return Task.findById(taskId).then((result, error) =>{

		if(error){
			console.log(error);
			return false;
		}
		else{
			result.status = "Complete";

			return result.save().then((updatedTaskStatus, updateErr) =>{

				if(updateErr){
					console.log(updateErr)
					return false
				}
				else{
					return updatedTaskStatus;
				}
			})
		}
	})
}
