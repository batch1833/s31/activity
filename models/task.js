// Create schema use mongoose // schema and model creation here

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "Pending"
	}
})

module.exports = mongoose.model("Task", taskSchema); //will contain in one variable 