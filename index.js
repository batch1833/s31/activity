// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

//This allow us to use all the routes defined in the "taskRoute.js"
const taskRoute = require("./routes/taskRoutes");

//Server setup
const app = express();
const port = 3001;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//MondoDB Connection string
mongoose.connect("mongodb+srv://admin:admin@cluster0.mmogcml.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});


//Set the notification for connection or failure

let db = mongoose.connection;

//Notify on error
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

//Add the task route
//Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
//localhost:3001/tasks
app.use("/tasks", taskRoute);


//Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));

/*
	Separation of Concerns

	Model folder
	- contains the object Schemas and defifnes the object structure and content
	
	Controllers Folder
	-contain the function and business logic if our JS application
	-meaning all the operations it can do, will be place here.

	Routes folder
	-Contain all the endpoints and assign the http methods for our application
	app.get("/", )
	-what happen is we separate the routes such that the server or "index.js" only contains information on the server.

	JS modules
	require => to include a specific module/package.
		 // gamit nya para ma include sa file
	
	export.module => to treat a value as a "package that cana be used by other files."

	Flow of exports and require:
	export models > gagamit/require in controllers
	export controllers > gagamit/requires in routes
	export routes > gagamit/require in server (index.js)

*/